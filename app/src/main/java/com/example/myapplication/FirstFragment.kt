package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.adapter.UserInformationAdapter
import com.example.myapplication.databinding.FragmentFirstBinding
import com.example.myapplication.netmodule.NetworkCallRepositoryImpl
import com.example.myapplication.netmodule.RetrofitClient
import com.example.myapplication.viewModel.UserInformationViewModel
import com.example.myapplication.viewModel.UserInformationViewModelFactory

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private lateinit var viewModel: UserInformationViewModel
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val retrofit = RetrofitClient.getInstance()
    private val adapter = UserInformationAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(
            this, UserInformationViewModelFactory(
                NetworkCallRepositoryImpl(retrofit)
            )
        ).get(UserInformationViewModel::class.java)
        binding.recyclerView.adapter = adapter
        viewModel.userInformationList.observe(viewLifecycleOwner, Observer {
            adapter.setUserData(it)
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Log.d("Error", it)
        })
        viewModel.getAllUserData()


        return binding.root

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}