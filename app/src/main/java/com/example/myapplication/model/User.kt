package com.example.myapplication.model

data class User(
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
)
