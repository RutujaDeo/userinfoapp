package com.example.myapplication.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.AdapterUserInfoBinding
import com.example.myapplication.model.User

class UserInformationAdapter : RecyclerView.Adapter<MainViewHolder>() {

    var userInformationList = mutableListOf<User>()
    @SuppressLint("NotifyDataSetChanged")
    fun setUserData(userInfo: List<User>) {
        this.userInformationList = userInfo.toMutableList()
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding = AdapterUserInfoBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val user = this.userInformationList[position]
        holder.binding.name.text = user.title

    }

    override fun getItemCount(): Int {
        return this.userInformationList.size
    }
}

class MainViewHolder(val binding: AdapterUserInfoBinding) : RecyclerView.ViewHolder(binding.root) {

}