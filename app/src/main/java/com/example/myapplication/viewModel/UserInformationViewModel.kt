package com.example.myapplication.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.model.User
import com.example.myapplication.netmodule.NetworkCallRepositoryImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserInformationViewModel constructor(
    private val networkCall: NetworkCallRepositoryImpl
) : ViewModel() {

    val userInformationList = MutableLiveData<List<User>>()
    val errorMessage = MutableLiveData<String>()

    fun getAllUserData() {

        val response = networkCall.getUserData()
        response.enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                userInformationList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}