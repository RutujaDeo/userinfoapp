package com.example.myapplication.netmodule

class NetworkCallRepositoryImpl constructor(
    private val retrofitService: RetrofitClient
) {

    fun getUserData() = retrofitService.getAllUserInformation()
}
