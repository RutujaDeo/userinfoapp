package com.example.myapplication.netmodule

import com.example.myapplication.model.User
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface RetrofitClient {

    @GET("/todos")
    fun getAllUserInformation(): Call<List<User>>

    companion object {

        var retrofitService: RetrofitClient? = null

        fun getInstance(): RetrofitClient {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://jsonplaceholder.typicode.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitClient::class.java)
            }
            return retrofitService!!
        }
    }
}